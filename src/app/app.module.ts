import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AccountProvider } from '../providers/auth/auth';
import { DatePipe } from '@angular/common';
import { ProductProvider } from '../providers/product/product';
import { OrderProvider } from '../providers/order/order';
import { CategoryProvider } from '../providers/category/category';
import { AddressProvider } from '../providers/address/address';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyBQdUFjuqYxFPFu_UxqoAOQF4ZEDaFEwO0",
      authDomain: "fabricacodigovendas.firebaseapp.com",
      databaseURL: "https://fabricacodigovendas.firebaseio.com",
      projectId: "fabricacodigovendas",
      storageBucket: "fabricacodigovendas.appspot.com",
      messagingSenderId: "739044368273"
    }),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AccountProvider,
    DatePipe,
    ProductProvider,
    OrderProvider,
    CategoryProvider,
    AddressProvider
  ]
})
export class AppModule {}
