import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { DatePipe } from '@angular/common';

@Injectable()
export class OrderProvider {
  private PATH: string = 'pedidos/';
  private PATH_CART: string = 'carrinho/';

  public static STATUS = {
    SENT: 0,
    CONFIRMED: 1,
    OUT_FOR_DELIVERY: 2,
    DELIVERED: 3
  };

  public static PAYMENT_TYPE = {
    MONEY: 1,
    CARD: 2
  };

  constructor(private db: AngularFireDatabase, private auth: AngularFireAuth, private dateFormat: DatePipe) {
    if (!this.auth.auth.currentUser) return;

    this.PATH_CART += this.auth.auth.currentUser.uid + '/';
  }

  public getAll() {
    return this.db.list(this.PATH)
      .snapshotChanges()
      .map(changes => {
        return changes.map(m => ({ key: m.key, ...m.payload.val() }));
      });
  }

  public getAllOpen() {
    return this.db.list(this.PATH, ref => ref.orderByChild('userStatus').endAt(this.auth.auth.currentUser.uid + '_' + OrderProvider.STATUS.OUT_FOR_DELIVERY))
      .snapshotChanges()
      .map(changes => {
        return changes.map(m => ({ key: m.key, ...m.payload.val() }));
      });
  }

  public createOrder(order: any) {
    return new Promise((resolve) => {
      let number = '#' + this.dateFormat.transform(new Date(), 'ddMMyyyyHHmmss');
      let date = this.dateFormat.transform(new Date(), 'dd/MM/yyyy');

      const cart = this.getCartItems().subscribe(cartItems => {
        cart.unsubscribe();

        let total = 0;
        let items = [];
        // Pegando os itens do pedido
        cartItems.forEach(item => {
          items.push(item);
          total += item.total;
        });

        // Criando o pedido
        let orderToSave = {
          number: number,
          status: OrderProvider.STATUS.SENT, // pedido enviado
          date: date,
          paymentType: order.paymentType,
          changeTo: order.changeTo,
          cardType: order.cardType,
          address: order.address,
          userId: this.auth.auth.currentUser.uid,
          userName: this.auth.auth.currentUser.displayName,
          userStatus: this.auth.auth.currentUser.uid + '_' + OrderProvider.STATUS.SENT, // Tecnica para filtro de varios campos
          total: total,
          items: items,
        };

        let orderRef = this.db.list(this.PATH);
        orderRef.push(orderToSave)
          .then(() => {
            //nesse momento eu limpo o carrinho
            this.db.object(this.PATH_CART).remove();
            resolve();
          });
      });
    });
  }

  // Mandar os métodos que tem haver com o carrinho do compras para um novo provider
  // Tecnica de organização de código
  public getCartTotalItems() {
    return this.db.list(this.PATH_CART).snapshotChanges().map(changes => {
      return changes.length;
    });
  }

  public getCartTotalValue() {
    return this.db.list(this.PATH_CART).snapshotChanges().map(changes => {
      let total = 0;
      changes.forEach(item => {
        total += item.payload.val().total;
      })
      return total;
    });
  }

  public getCartItems() {
    return this.db.list(this.PATH_CART).snapshotChanges().map(changes => {
      return changes.map(m => ({ key: m.key, ...m.payload.val() }));
    });
  }

  public addProduct(item: any) {
    return this.db.list(this.PATH_CART).push(item);
  }

  public updateProduct(item: any) {
    return this.db.object(this.PATH_CART + item.key).update({ qtd: item.qtd, total: item.total });
  }

  public removeProduct(item: any) {
    return this.db.object(this.PATH_CART + item.key).remove();
  }

  getStatusName(status: number) {
    switch (status) {
      case OrderProvider.STATUS.SENT:
        return 'Aguardando confirmação';
      case OrderProvider.STATUS.CONFIRMED:
        return 'Em preparação';
      case OrderProvider.STATUS.OUT_FOR_DELIVERY:
        return 'Saiu para entrega';
      case OrderProvider.STATUS.DELIVERED:
        return 'Entregue';
    }
  }

  getPaymentType(paymentType: number) {
    switch (paymentType) {
      case OrderProvider.PAYMENT_TYPE.MONEY:
        return 'Dinheiro';
      case OrderProvider.PAYMENT_TYPE.CARD:
        return 'Cartão de crédito/débido';
    }
  }
}
