import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Injectable()
export class AccountProvider {
  private PATH = 'clientes/';

  constructor(public auth: AngularFireAuth, private db: AngularFireDatabase) { }

  public createAccount(user: any) {
    return new Promise((resolve, reject) => {
      this.auth.auth.createUserWithEmailAndPassword(user.email, user.password)
        .then((firebaseUser: firebase.User) => {
          this.db.object(this.PATH + firebaseUser.uid).set({ emailVerified: false });
          firebaseUser.updateProfile({ displayName: user.name, photoURL: null });
          firebaseUser.sendEmailVerification();

          this.signOut();
          resolve();
        })
        .catch(e => {
          reject(this.handlerError(e));
        });
    });
  }

  public login(user: any) {
    return new Promise((resolve, reject) => {
      this.auth.auth.signInWithEmailAndPassword(user.email, user.password)
        .then((firebaseUser: firebase.User) => {
          // Se o email do usuario foi verificado eu atualizo a informação
          if (firebaseUser.emailVerified) {
            this.db.object(this.PATH + firebaseUser.uid).update({ emailVerified: true });
          }
          resolve({ emailVerified: firebaseUser.emailVerified });
        })
        .catch(e => {
          reject(this.handlerError(e));
        });
    });
  }

  public sendPasswordResetEmail(email: string) {
    return new Promise((resolve, reject) => {
      this.auth.auth.sendPasswordResetEmail(email)
        .then(() => {
          resolve();
        })
        .catch(e => {
          reject(this.handlerError(e));
        });;
    });
  }

  public getUserData() {
    let user = { name: '', email: '' };
    if (this.auth.auth.currentUser) {
      user.name = this.auth.auth.currentUser.displayName,
        user.email = this.auth.auth.currentUser.email
    }

    return user;
  }

  public signOut() {
    this.auth.auth.signOut();
  }

  private handlerError(error: any) {
    let message = '';
    if (error.code == 'auth/email-already-in-use') {
      message = 'O e-mail informado já está sendo usado.';
    } else if (error.code == 'auth/invalid-email') {
      message = 'O e-mail informado é inválido.';
    } else if (error.code == 'auth/weak-password') {
      message = 'A senha informada é muito fraca.';
    } else if (error.code == 'auth/user-not-found') {
      message = 'Usuário não encontrado.';
    } else if (error.code == 'auth/wrong-password') {
      message = 'Usuário/senha inválido(s).';
    } else {
      message = 'Ocorreu algum erro, por favor tente novamente.';
    }

    return message;
  }
}
