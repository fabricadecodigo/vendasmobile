import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';

@Injectable()
export class AddressProvider {
  private PATH = 'clientesEnderecos/';

  constructor(private db: AngularFireDatabase, public auth: AngularFireAuth) {
    if (!this.auth.auth.currentUser) return;

    this.PATH += this.auth.auth.currentUser.uid;
  }

  public getAll() {
    return this.db.list(this.PATH)
      .snapshotChanges()
      .map(changes => {
        return changes.map(m => ({ key: m.key, ...m.payload.val() }));
      });
  }

  public save(item: any, key: string) {
    return new Promise((resolve, reject) => {
      if (item.key) {
        this.db.list(this.PATH).update(key, item)
          .then(() => resolve())
          .catch((e) => reject(e));
      } else {
        this.db.list(this.PATH).push(item)
          .then(() => resolve());
      }
    });
  }

  public remove(key: string) {
    return this.db.list(this.PATH).remove(key);
  }
}
