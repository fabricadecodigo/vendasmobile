import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class ProductProvider {
  private PATH = 'produtos/';

  constructor(private db: AngularFireDatabase) { }

  public getAll(categoryKey: string) {
    return this.db.list(this.PATH, ref => {
      if (categoryKey) {
        return ref.orderByChild('categoryKey').equalTo(categoryKey)
      } else {
        return ref.orderByChild('name')
      }
    }).snapshotChanges().map(changes => {
      return changes.map(m => ({ key: m.key, ...m.payload.val() }));
    });
  }
}
