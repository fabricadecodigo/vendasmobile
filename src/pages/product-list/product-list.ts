import { OrderProvider } from './../../providers/order/order';
import { CategoryProvider } from './../../providers/category/category';
import { ProductProvider } from './../../providers/product/product';
import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-product-list',
  templateUrl: 'product-list.html',
})
export class ProductListPage {
  items: Observable<any[]>;
  categories: Observable<any[]>;
  category: string;
  showCartButton = false;

  constructor(public navCtrl: NavController, private productProvider: ProductProvider,
    private categoryProvider: CategoryProvider, private orderProvider: OrderProvider) {

    this.categories = this.categoryProvider.getAll();
    this.items = this.productProvider.getAll(null);
    this.orderProvider.getCartTotalItems()
      .subscribe(total => {
        this.showCartButton = (total > 0);
      })
  }

  addProduct(item: any) {
    console.log(item);
    this.navCtrl.push('ItemEditPage', { product: item });
  }

  getProducts() {
    this.items = this.productProvider.getAll(this.category);
  }

  public openCart() {
    this.navCtrl.push('ItemListPage');
  }
}
