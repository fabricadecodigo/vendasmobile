import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tabs: any[] = [];

  constructor() {
    this.tabs.push({ component: 'ProductListPage', title: 'Produtos', icon: 'list' });
    this.tabs.push({ component: 'OrderListPage', title: 'Pedidos', icon: 'basket' });
    this.tabs.push({ component: 'UserinfoPage', title: 'Eu', icon: 'person' });
  }
}
