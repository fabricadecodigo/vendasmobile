import { OrderProvider } from './../../providers/order/order';
import { Observable } from 'rxjs/Observable';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-order-list',
  templateUrl: 'order-list.html',
})
export class OrderListPage {
  items: Observable<any[]>;

  constructor(public navCtrl: NavController, public navParams: NavParams, private orderProvider: OrderProvider) {
    this.items = this.orderProvider.getAllOpen();
  }

  getAllOrders($event) {
    if ($event.checked) {
      this.items = this.orderProvider.getAll();
    } else {
      this.items = this.orderProvider.getAllOpen();
    }
  }

  getPaymentType(paymentType: number) {
    return this.orderProvider.getPaymentType(paymentType);
  }

  getStatusName(status: number) {
    return this.orderProvider.getStatusName(status);
  }
}
