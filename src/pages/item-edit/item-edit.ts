import { OrderProvider } from './../../providers/order/order';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { minValueValidator } from '../../directives/min-value/min-value';

@IonicPage()
@Component({
  selector: 'page-item-edit',
  templateUrl: 'item-edit.html',
})
export class ItemEditPage {
  title: string;
  description: string;
  price: number;
  total: number;
  form: FormGroup;
  private product: any;

  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    private toast: ToastController, private formBuilder: FormBuilder,
    private orderProvider: OrderProvider) {

    this.setupPage();
    this.createForm();
  }

  private setupPage() {
    this.product = this.navParams.data.product;
    this.title = this.product.name;
    this.description = this.product.description;
    this.price = this.product.price;
    this.total = this.product.price;
  }

  private createForm() {
    this.form = this.formBuilder.group({
      productKey: [this.product.key],
      productName: [this.product.name],
      description: [this.product.description],
      qtd: [1, [Validators.required, minValueValidator(1)]],
      price: [this.product.price],
      obs: [''],
      total: [this.product.price]
    });
  }

  addQtd() {
    let qtd = this.form.value.qtd;
    qtd++;

    this.form.controls['qtd'].setValue(qtd);
    this.updateTotal(qtd);
  }

  removeQtd() {
    let qtd = this.form.value.qtd;

    qtd--;
    // forço a quantidade nunca ser menor que 0
    if (qtd < 0)
      qtd = 0;

    this.form.controls['qtd'].setValue(qtd);
    this.updateTotal(qtd);
  }

  qtdChanged($event) {
    this.updateTotal($event.value);
  }

  private updateTotal(qtd: number) {
    this.total = this.getTotal(qtd, this.price);
    this.form.controls['total'].setValue(this.total);
  }

  private getTotal(price: number, qtd: number) {
    return qtd * price;
  }

  onSubmit() {
    if (this.form.valid) {
      this.orderProvider.addProduct(this.form.value);
      this.toast.create({ message: 'Produto adicionado com sucesso.', duration: 3000 }).present();
      this.navCtrl.pop();
    }
  }
}
