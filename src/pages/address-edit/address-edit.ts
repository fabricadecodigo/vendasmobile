import { AddressProvider } from './../../providers/address/address';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-address-edit',
  templateUrl: 'address-edit.html',
})
export class AddressEditPage {
  form: FormGroup;
  title: string;
  item: any;
  estados: string[];

  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    private toast: ToastController, private formBuilder: FormBuilder,
    private addressProvider: AddressProvider) {

    this.item = this.navParams.data.item || {};
    this.loadEstados();
    this.setupPageTitle();
    this.createForm();
  }

  private setupPageTitle() {
    if (this.navParams.data.item) {
      this.title = 'Alterando endereço';
    } else {
      this.title = 'Novo endereço';
    }
  }

  private createForm() {
    this.form = this.formBuilder.group({
      key: [this.item.key],
      logradouro: [this.item.logradouro, Validators.required],
      numero: [this.item.numero, Validators.required],
      complemento: [this.item.complemento],
      bairro: [this.item.bairro, Validators.required],
      cidade: [this.item.cidade, Validators.required],
      uf: [this.item.uf || '', Validators.required]
    });
  }

  loadEstados() {
    this.estados = [
      'AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO',
      'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI',
      'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'
    ];
  }

  onSubmit() {
    if (this.form.valid) {
      this.addressProvider.save(this.form.value, this.item.key)
        .then(() => {
          this.toast.create({ message: 'Endereço salvo com sucesso.', duration: 3000 }).present();
          this.navCtrl.pop();
        }).catch(() => {
          this.toast.create({ message: 'Ocorreu algum erro ao salvar, por favor tente novamente.', duration: 3000 }).present();
        })
    }
  }
}
