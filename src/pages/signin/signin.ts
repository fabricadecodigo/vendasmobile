import { AccountProvider } from './../../providers/auth/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {
  form: FormGroup;

  constructor(
    public navCtrl: NavController, private formBuilder: FormBuilder,
    private authProvider: AccountProvider, private toast: ToastController) {

    this.createForm();
  }

  private createForm() {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  onSubmit() {
    if (this.form.valid) {
      this.authProvider.login(this.form.value)
        .then((user: any) => {
          if (user.emailVerified) {
            this.navCtrl.setRoot('TabsPage');
          } else {
            this.toast.create({ message: 'Seu e-mail ainda não foi verificado. Por favor acesse seu e-mail e clique no link para verificar conta.', duration: 3000 }).present();
          }
        })
        .catch(message => {
          this.toast.create({ message: message, duration: 3000 }).present();
          console.error(message);
        });
    }
  }

  public forgotPassword() {
    this.navCtrl.push('ForgotPasswordPage');
  }

  public signup() {
    this.navCtrl.push('SignupPage');
  }
}
