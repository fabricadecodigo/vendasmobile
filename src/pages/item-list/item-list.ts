import { Observable } from 'rxjs/Observable';
import { OrderProvider } from './../../providers/order/order';
import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-item-list',
  templateUrl: 'item-list.html',
})
export class ItemListPage {
  items: Observable<any[]>;
  total: number;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, private orderProvider: OrderProvider) {
    this.items = this.orderProvider.getCartItems();
    this.total = 0;
    this.orderProvider.getCartTotalValue().subscribe((totalValue: number) => {
      this.total = totalValue;
    });
  }

  addQtd(item: any) {
    let qtd = item.qtd;
    qtd++;
    this.updateTotal(qtd, item);
  }

  removeQtd(item: any) {
    let qtd = item.qtd;
    qtd--;
    if (qtd <= 0) {
      this.confirmRemoveItem(item);
    } else {
      this.updateTotal(qtd, item);
    }
  }

  private updateTotal(qtd: number, item: any) {
    let total = 0;
    total = qtd * item.price;
    item.qtd = qtd;
    item.total = total;
    this.orderProvider.updateProduct(item);
  }

  private confirmRemoveItem(item: any) {
    let confirm = this.alertCtrl.create({
      title: 'Remover o item?',
      message: 'Deseja remover o item ' + item.productName + '?',
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Remover item',
          handler: () => {
            this.orderProvider.removeProduct(item);
          }
        }
      ]
    });
    confirm.present();
  }

  public openPayment() {
    this.navCtrl.push('PaymentPage');
  }
}
