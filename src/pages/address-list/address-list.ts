import { AddressProvider } from './../../providers/address/address';
import { Observable } from 'rxjs/Observable';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-address-list',
  templateUrl: 'address-list.html',
})
export class AddressListPage {
  items: Observable<any[]>;
  selectAddressMode = false;

  constructor(
    public navCtrl: NavController, public navParam: NavParams,
    private addressProvider: AddressProvider, private toast: ToastController,
    public viewCtrl: ViewController) {

    this.items = this.addressProvider.getAll();
    this.selectAddressMode = this.navParam.data.selectAddressMode || false;
  }

  getAdreesText(item: any) {
    let address: '';
    address = item.logradouro;
    address += ', ' + item.numero;
    if (item.complemento) {
      address += ', ' + item.complemento;
    }
    address += ' - ' + item.bairro;
    address += ' - ' + item.cidade;
    address += '(' + item.uf + ')';
    return address;
  }

  newItem() {
    this.navCtrl.push('AddressEditPage');
  }

  editItem(item: any) {
    this.navCtrl.push('AddressEditPage', { item: item });
  }

  removeItem(key: string) {
    this.addressProvider.remove(key);
    this.toast.create({ message: 'Endereço removido com sucesso.', duration: 3000 }).present();
  }

  setSelectAddress(item: any) {
    if (this.selectAddressMode) {
      const address = this.getAdreesText(item);
      this.viewCtrl.dismiss({ address: address});
    }
  }
}
