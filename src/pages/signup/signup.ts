import { AccountProvider } from './../../providers/auth/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  form: FormGroup;

  constructor(
    public navCtrl: NavController, private formBuilder: FormBuilder,
    private authProvider: AccountProvider, private toast: ToastController) {

    this.createForm();
  }

  private createForm() {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.form.valid) {
      this.authProvider.createAccount(this.form.value)
        .then(() => {
          this.toast.create({ message: 'Conta criada com sucesso. Por favor confirme seu e-mail antes de efetuar o login.', duration: 3000 }).present();
          this.navCtrl.pop();
        })
        .catch(message => {
          this.toast.create({ message: message, duration: 3000 }).present();
        });
    }
  }

}
