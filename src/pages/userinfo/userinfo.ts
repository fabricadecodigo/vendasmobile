import { AccountProvider } from './../../providers/auth/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-userinfo',
  templateUrl: 'userinfo.html',
})
export class UserinfoPage {
  user: any = {};

  constructor(public navCtrl: NavController, public authProvider: AccountProvider) {
    this.user = this.authProvider.getUserData();
  }

  openMyAddresses() {
    this.navCtrl.push('AddressListPage');
  }

  signOut() {
    this.authProvider.signOut();
    this.navCtrl.parent.parent.setRoot('SigninPage');
  }
}
